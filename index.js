// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

	const getCube = 2 ** 3;

		message = `The cube of 2 is ${getCube}`;
		console.log(message);

// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.

	const address = [258, 'Washington Ave NW', 'California', 90011]

		console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`)

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

	const animal = {
		name: 'Lolong',
		type: 'saltwater crocodile',
		weight: 1075,
		measurement: '20 ft 3 in'
	}

		console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement}.`);

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.





	const numbers = [1,2,3,4,5];

	numbers.forEach((number) => {
		console.log(`${number}`);
	});

	const numbersTotal = [1,2,3,4,5];

	let result = numbersTotal.reduce((number, numbers) => {
		return number + numbers;
	});

	console.log(`${result}`);


// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

	class Dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	const myDog = new Dog()

		myDog.name = 'Frankie';
		myDog.age = 5;
		myDog.breed = 'Miniature Dachshund';

		console.log(myDog);